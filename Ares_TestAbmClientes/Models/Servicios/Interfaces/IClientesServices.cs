﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ares_TestAbmClientes.Models.Servicios
{
    public interface IClientesServices
    {
        IEnumerable<Cliente> ObtenerPrimeros(int max);
        IEnumerable<Cliente> ObtenerUltimos(int max);
        IEnumerable<Cliente> ObtenerPorMesAñoNacimiento(int año, int mes);
        Cliente ObtenerPorId(int Id);
        IEnumerable<Cliente> ObtenerEntreIds(int desde, int hasta);
    }
}
