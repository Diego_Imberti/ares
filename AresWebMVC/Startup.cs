using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AresWebMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            //services.AddTransient<IAdder, Calculadora>();
            //services.AddTransient<IOperationFormatter, OperationFormatter>();
            services.AddMemoryCache(); // Para Cache
            services.AddDistributedMemoryCache(); // Cache en Memoria: para variables de Sesion
            services.AddSession(); // Servicio de Sesion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(  IApplicationBuilder app, 
                                IWebHostEnvironment env, 
                                IConfiguration configuration,
                                ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger("EspiaDeEndpoints");
            app.UseSession(new SessionOptions()
            {
                Cookie =
                {
                    Name = ".AresCk",
                },
                IdleTimeout = TimeSpan.FromSeconds(15)
            }); ; // Variables de Sesion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            

            app.UseStatusCodePages();
            app.UseWelcomePage("/Tests");
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting(); // Mw que Decide
            // Logueo el Endpoint que pilla el Mw
            app.Use(async (ctx, next) => 
            {
                var endpoint = ctx.GetEndpoint();
                if (endpoint != null)
                {
                    var description = endpoint.ToString();
                    logger.LogInformation(description);
                }
                else
                {
                    logger.LogWarning("No hay un endpoint que coincida");
                }
                await next();
            });



            app.UseAuthorization();
            app.UseAuthentication();//cookies para usuario conectado

            // Mw que Ejecuta
            app.UseEndpoints(endpoints =>
            {
                // Para MVC
                endpoints.MapDefaultControllerRoute();
                //endpoints.MapGet("/", async ctx =>
                //{
                //    await ctx.Response.WriteAsync("Hola Raiz.!");
                //});
                //endpoints.MapGet("/one", async ctx =>
                //{
                //    await ctx.Response.WriteAsync("Hola ONE.!");
                //});
                //endpoints.MapGet("/otro/test", async ctx =>
                //{
                //    await ctx.Response.WriteAsync("Text");
                //})
                //.RequireAuthorization()
                //.RequireHost("localhost")
                //.WithDisplayName("Esto es un test DISPLAY")
                //.WithMetadata("Metadatos");

                //endpoints.MapGet("/red", async ctx =>
                //{
                //    ctx.Response.Redirect("/one");
                //});

                //endpoints.MapGet("visits", async (HttpContext context) => 
                //{
                //    var newCount = context.Session.GetInt32("count").GetValueOrDefault() + 1;
                //    context.Session.SetInt32("count", newCount);
                //    await context.Response.WriteAsync($"Visitas tuyas: {newCount}");
                //});
                //endpoints.MapGet("reset", async (HttpContext context) =>
                //{
                //    context.Session.SetInt32("count", 0);
                //    await context.Response.WriteAsync($"Visitas tuyas: RESET ");
                //});

                // Pillar una variable a una URL determinada
                //Consrtaints varios: int/min/max/date, etc (pagina 145)
                //endpoints.MapGet("/Hola/{nombre:alpha:minlength(2):maxlength(15)}/{apellido:int}/{pordefecto=1}", async ctx =>
                //{
                //    var name = ctx.GetRouteValue("nombre").ToString();
                //    var apell = ctx.GetRouteValue("apellido")?.ToString() ?? "";
                //    await ctx.Response.WriteAsync($"Hola {name} {apell}");
                //});



                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                //endpoints.MapGet("/", async context =>
                //{
                //    var name = (string)context.Request.Query["name"] ?? "Desconocido";
                //    await context.Response.WriteAsync($"Hola, {name}.!");
                //});

             
               
            });

        
        }
    }



}
