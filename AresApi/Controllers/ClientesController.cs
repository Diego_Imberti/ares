﻿using Ares_TestAbmClientes.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AresApi.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("Api/")]
    public class ClientesController : ControllerBase
    {



        [HttpGet("Create")]
        public IActionResult Create(Cliente cliente)
        {
            // Si decoramos con "ApiController", estas líneas ya no son necesarias
            //if (!ModelState.IsValid)
            //    return ValidationProblem(ModelState);

            var clienteAgregar = new Cliente();

            // Agrego
            clienteAgregar.Nombre = cliente.Nombre;
            clienteAgregar.Apellidos = cliente.Apellidos;


            return Created("",Newtonsoft.Json.JsonConvert.SerializeObject(clienteAgregar));
        }

        // Esto es mejor un TASK - AWAIT
        [HttpGet("Get/{id}")]
        public ActionResult<Cliente> Get(int id)
        {
            if (id == 0)
                return NotFound();

            return new Cliente();

        }

        //// GET: ClientesController/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: ClientesController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: ClientesController/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ClientesController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: ClientesController/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ClientesController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: ClientesController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
