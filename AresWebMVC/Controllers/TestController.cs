﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AresWebMVC.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        // Responde a https://localhost:5001/test/hola/5?nombre=pepe
        public IActionResult Hola(string id,string nombre) 
        {
            var idRecibido = id ?? "Sin Datos";
            return Content($"Hola desde AspNetCore MVC, {idRecibido}, {nombre}");
        }
    }
}
