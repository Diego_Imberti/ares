﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ares_TestAbmClientes.Models
{
    public class Cliente
    {
        public int Id { get; set; }
      
        
        [Required, MinLength(4), MaxLength(50)]
        public string Nombre { get; set; }
        [Required (AllowEmptyStrings = false,ErrorMessage = "Debe de picar alguna cosilla")]
        public string Apellidos { get; set; }
    
        [Required, Range(18,99, ErrorMessage = "La edad debe de estar entre 18 y 99 años")]
        public int Edad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Sexo { get; set; }
        public TimeSpan TimeSpan { get; set; }
        [Required (ErrorMessage = "Macho, pica el email")]
        [EmailAddress (ErrorMessage ="Chato, el email esta mal")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Macho, pica el email2"), Compare(nameof(Email), ErrorMessage = "Los correos deben de Coincidir")]
        [EmailAddress(ErrorMessage = "Chato, el email esta mal")]
        public string RepitaEmail { get; set; }
    }
}
