﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ares_TestAbmClientes.Models.Servicios
{
    public class ClientesServices : IClientesServices
    {
        // Repo en memoria para pruebas
        private static readonly List<Cliente> Clientes;

        // Poblado inicial de datos en el constructor
        static ClientesServices()
        {
            Clientes = new List<Cliente>
            {
                new Cliente()
                { Id = 1,Nombre = "Diego", Apellidos = "Imberti",Edad = 48,FechaNacimiento = Convert.ToDateTime("14/04/1973") ,Sexo = "M" },
                new Cliente()
                { Id = 2,Nombre = "Nefer", Apellidos = "Del Pozo",Edad = 32,FechaNacimiento = Convert.ToDateTime("09/12/1989") ,Sexo = "F" },
                new Cliente()
                { Id = 3,Nombre = "Arya", Apellidos = "Imberti",Edad = 2,FechaNacimiento = Convert.ToDateTime("13/10/2020") ,Sexo = "F" }
            };

            // Agrego algunos más

            for (int i = 4; i < 15; i++)
            {
                Clientes.Add(
                             new Cliente()
                             { Id = i, Nombre = $"Diego {i}", Apellidos = $"Imberti {i}", Edad = 30 + i, FechaNacimiento = new DateTime(1989, 09, 12).AddDays(i), Sexo = "M" }
                            );
            }
        }

        // Miembros del Interface

        public IEnumerable<Cliente> ObtenerEntreIds(int desde, int hasta)
        {
            var ListaClientes = from cliente in Clientes
                                where cliente.Id >= desde && cliente.Id <= hasta
                                select cliente;
            return ListaClientes.ToList();
        }

        public Cliente ObtenerPorId(int Id)
        {
            return Clientes.FirstOrDefault(
                Cliente => Cliente.Id.Equals(Id));
        }

        public IEnumerable<Cliente> ObtenerPorMesAñoNacimiento(int año, int mes)
        {
            var ListaClientes = from cliente in Clientes
                                where cliente.FechaNacimiento.Month == mes && cliente.FechaNacimiento.Year == año
                                orderby cliente.FechaNacimiento descending
                                select cliente;
            return ListaClientes.ToList(); 
        }

        public IEnumerable<Cliente> ObtenerPrimeros(int max)
        {
            var ListaClientes = from cliente in Clientes
                                orderby cliente.Id descending
                                select cliente;
            return ListaClientes.Take(max).ToList();
        }

        public IEnumerable<Cliente> ObtenerUltimos(int max)
        {
            var ListaClientes = from cliente in Clientes
                                orderby cliente.Id ascending
                                select cliente;
            return ListaClientes.Take(max).ToList();
        }

     


    }

}