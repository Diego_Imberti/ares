﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AresApi.Controllers
{

    public class TokenController : Controller
    {
        [HttpPost("/Token/{username}/{password}")]
        public IActionResult GetToken(string username, string password)
        {
            if (username == password)
            {
                Token token = new Token();
                token.access_token = GenerarToken(username);
                token.email = "d";
                return Ok(token);
            }
            else
                return Unauthorized();
        }

        private string GenerarToken(string username)
        {
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, username)
            };

            var TokenSecretKey = Encoding.ASCII.GetBytes("DiegoDiegoTokenToken");

            var key = new SymmetricSecurityKey(TokenSecretKey);

            var creds = new SigningCredentials(key,
                                SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "OchoVeinte.com",
                audience: "AresApi.com",
                claims: claims,
                expires: DateTime.Now.AddSeconds(10),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string email { get; set; }
    }
}
