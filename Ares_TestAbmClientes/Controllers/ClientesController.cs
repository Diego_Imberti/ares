﻿using Ares_TestAbmClientes.Models.Servicios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ares_TestAbmClientes.Controllers
{
    [Route("Clientes")]
    public class ClientesController : Controller
    {
        // Para leer del config: 353
        // Para variables de Sesion (355)
        protected ISession Session => HttpContext.Session;


        // Para la inyección de Dependencias
        private readonly IClientesServices _clientesServices;

        // Constructor, que recibe la inyección de dependencias
        public ClientesController(IClientesServices clientesServices)
        {
            _clientesServices = clientesServices;
        }

        [HttpGet("Listado", Name = "ClientesListado")]
        public IActionResult Index(int cantidad = 10)
        {
            IEnumerable<Models.Cliente> clientes = _clientesServices.ObtenerUltimos(cantidad);
            return View("Index", clientes); // Index, no es necesario si la vista coincide con el Nombre del Método
        }

        // get ? post ? put ? = (545)

        [HttpGet("Clientes/Ver/{Id}", Name = "ClienteDetalle")]
        //[Authorize]
        public IActionResult VerCliente(int Id)
        {
            Models.Cliente cliente = _clientesServices.ObtenerPorId(Id);
            if (cliente != null)
                return View(cliente);

            return NotFound();
        }

        [HttpGet("Editar/{Id}", Name = "ClienteEditarPorId")]
       // [Authorize]
        public IActionResult EditarCliente(int Id)
        {
            Models.Cliente cliente = _clientesServices.ObtenerPorId(Id);
            if (cliente == null)
                return NotFound();
            else
                return View(cliente);
        }

        [HttpPost("Editar/{Id}", Name ="ClienteGuardarEditadoPorId")]
        public IActionResult EditarCliente(Models.Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Error_Edad"] = "Error en Edad";
                return View(cliente);
            }

            if (cliente == null)
                return NotFound();

            System.Threading.Thread.Sleep(10000);

            // Redireccion a otro lado
            return LocalRedirect("/Home");


            // Redirecciones a acciones (364)

            // Para retornar al Indice del listado
            //IEnumerable<Models.Cliente> clientes = _clientesServices.ObtenerUltimos(10);
            //return View("Index", clientes); // Index, no es necesario si la vista coincide con el Nombre del Método
                                            //  return View(cliente);
        }
    }
}